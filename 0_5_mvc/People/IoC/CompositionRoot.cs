﻿using LightInject;
using People.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace People.IoC
{
    public class CompositionRoot : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.RegisterApiControllers();
            serviceRegistry.RegisterControllers();

            // register the InMemoryRepository
            serviceRegistry.Register<IRepository, InMemoryRepository>(new PerContainerLifetime());
            // registery the injection on contruction
            serviceRegistry.RegisterConstructorDependency(
                (factory, info) => factory.GetInstance<Type, IRepository>(info.Member.DeclaringType));

        }
    }
}