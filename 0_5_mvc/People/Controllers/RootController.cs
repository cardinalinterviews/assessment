﻿using People.IoC;
using People.Repositories;
using System.Web.Mvc;

namespace People.Controllers
{
    public class RootController : Controller
    {
        protected readonly IRepository _repo;


        public RootController(IRepository repo)
        {
            _repo = repo;

            ViewBag.Successes = 0;
            ViewBag.Failures = 0;
        }
    }
}