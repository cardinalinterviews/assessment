﻿using People.IoC;
using People.Models;
using People.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace People.Controllers
{
    public class PeopleController : RootController
    {        
        public PeopleController(IRepository repo)
            :base(repo)
        {

        }

        [HttpGet]
        public ActionResult Index()
        {
            var result = new List<PersonFormModel>();
            
            return View(result);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new PersonFormModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PersonFormModel person)
        {
         

            return Redirect("Index");
        }

    }
}
