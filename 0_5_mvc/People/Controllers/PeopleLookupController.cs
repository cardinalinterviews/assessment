﻿using People.IoC;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToolBox.Integration.PCubed;
using ToolBox.Integration.PCubed.Domain;

namespace People.Controllers
{
    public class PeopleLookupController : ApiController
    {


        public PeopleLookupController()
        {
          
        }
        
        /// <summary>
        ///  GET api/PeopleLookup/{idnumber}
        /// </summary>
        /// <param name="id">ID Number</param>
        /// <returns>Returns a PersonFormModel</returns>
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    return BadRequest("Required or Invalid Id");
                }

                var url = System.Configuration.ConfigurationManager.AppSettings["ConsumerViewApiUrl"];
                var consumerViewApiUsername = System.Configuration.ConfigurationManager.AppSettings["ConsumerViewApiUsername"];
                var consumerViewApiPassword = System.Configuration.ConfigurationManager.AppSettings["ConsumerViewApiPassword"];

                RestClient client = new RestClient(url);
                client.Authenticator = new HttpBasicAuthenticator(consumerViewApiUsername, consumerViewApiPassword);

                var service = new ConsumerViewService(client);
                var query = new ConsumerViewQuery(id);

                var responce = service.Search(query);
                if (responce.StatusCode != HttpStatusCode.OK || responce.Data.ResponseStatusCode == HttpStatusCode.NotFound)
                {
                    return NotFound();
                }

                // only grab the first entry
                var person = responce.Data.IndividualData.Individuals.Individual[0].PCubed;
                //Convert to model
                var result = new Models.PersonFormModel {
                    IDNumber = person.Header.IDNumber,
                    AddressLine1 = person.Detail.AddressLine1,
                    AddressLine2 = person.Detail.AddressLine2,
                    AddressPostCode = person.Detail.AddressPostCode,
                    AddressProvince = person.Detail.AddressProvince,
                    AddressSuburb = person.Detail.AddressSuburb,
                    AddressTownCity = person.Detail.AddressTownCity,
                    FirstName = person.Header.FirstName,
                    Surname = person.Header.Surname
                };   

                return Ok(result);
            }
            catch
            {
                // log exception
                throw;
            }
        }
    }
}