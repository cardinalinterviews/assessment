What is expected in this test
-----------------------------
This test measures your CSS skills

You are required to change the existing css and html page (page.htm) to have a similar layout, structure and style to the screenshot (result.png). 

Feel free to change the HTML, but do not use tables to achieve the required layout (there is no need to use tables)
