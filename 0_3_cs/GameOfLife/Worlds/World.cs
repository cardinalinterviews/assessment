﻿using System.Collections.Generic;
using System.Linq;
using GameOfLife.Printers;
using GameOfLife.Rules;
using GameOfLife.Worlds.Cells;
using Common.Logging;

namespace GameOfLife.Worlds
{
    public class World
    {
        private static readonly ILog log = LogManager.GetLogger<World>();

        private readonly List<WorldRow> _rows = new List<WorldRow>();

        public void AddRow()
        {
            _rows.Add(new WorldRow(_rows.Count));
        }

        public void AddCell(bool alive)
        {
            _rows.Last().AddCell(alive);
        }

        public IEnumerable<WorldRow> Rows
        {
            get
            {
                return _rows;
            }
        }

        public IEnumerable<WorldCell> GetNeighbors(int rowNumber, int cellNumber)
        {
            var result = new List<WorldCell>();

          

            return result;
        }

        public void Run(List<IGameOfLifeRule> rules)
        {
            foreach (var rule in rules)
                rule.Apply(this);

            foreach (var row in _rows)
                row.ApplyTransitions();
        }

        public void Print(List<IOutputWorldState> outputGenerators)
        {
            foreach (var output in outputGenerators)
            {
                output.Output(this);
            }
        }

    }
}
