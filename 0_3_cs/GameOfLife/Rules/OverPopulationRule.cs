﻿using System;
using System.Linq;
using Common.Logging;
using GameOfLife.Worlds;
using GameOfLife.Worlds.Cells;

namespace GameOfLife.Rules
{
    public class OverPopulationRule : IGameOfLifeRule
    {
        private static readonly ILog log = LogManager.GetLogger<OverPopulationRule>();

        public void Apply(World world)
        {
            foreach (var row in world.Rows)
            {
                ApplyRule(world, row);
            }
        }

        private void ApplyRule(World world, WorldRow row)
        {
            var baseType = typeof(AliveState);
            foreach (var cell in row.Cells)
            {
                var neighbours = world.GetNeighbors(row.RowNumber, cell.CellNumber);
                var livingCells = neighbours.Count(n => n.State.GetType() == baseType);

            }
        }

    }
}